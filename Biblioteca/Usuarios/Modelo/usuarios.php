<?php

require_once('../../conexion.php');
session_start();
class Usuarios extends Conexion{
    
    public function __construct(){
        $this->db = parent::__construct();
    }

    public function login($documento, $password){
        $statement = $this->db->prepare("SELECT * FROM tblUsuarios where bgnDocumento = :documento AND varClave = :password");
        $statement->bindParam(':documento', $documento);
        $statement->bindParam(':password', $password);
        $statement->execute();
        if($statement->rowCount() == 1){
            $result = $statement->fetch();
            $_SESSION['DOCUMENTO'] = $result['bgnDocumento'];
            $_SESSION['NOMBRE'] = $result['varNombre'];
            $_SESSION['ROL'] = $result['varRol'];
            return true;
        }
        return false;    
    }
    public function getDocumento(){
        return $_SESSION['DOCUMENTO'];
    }
    public function getNombre(){
        return $_SESSION['NOMBRE'];
    }
    public function getRol(){
        return $_SESSION['ROL'];
    }
    public function validarSession(){
        if ($_SESSION['DOCUMENTO'] == null){
            header('Location: ../../index.php');
        }
    }
    public function validarSessionBibliotecario(){
        if ($_SESSION['DOCUMENTO'] != null){
            if ($_SESSION['ROL']== 'Bibliotecario')
            {
                header('Location: ../../Bibliotecario/Vista/index.php');
            }
            else{
                header('Location: ../../Cliente/Vista/index.php');
            }
        }
    }
}

?>
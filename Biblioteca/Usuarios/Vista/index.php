<?php
session_start();
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
   
    <link rel="stylesheet" href="..\..\bootstrap\css\bootstrap.min.css">

    <link rel="stylesheet" href="..\..\bootstrap\css\app.css">
    <title>Biblioteca</title>
</head>
<body >
<nav class="navbar navbar-light bg-light fixed-top navbar-expand-md">
        <a href="../../index.php" class="navbar-brand posicion-search">Biblioteca</a>
        <button class="navbar-toggler" type="button" data-toggle= "collapse" data-target="#listarNav" aria-expanded="false" aria-controls="navbarNAvDropDown"><span class="navbar-toggler-icon"></span></button>
        <div class="collapse navbar-collapse " id="listarNav">
          
        </div>
    </nav>
    <div class="container">
        <center>
            <br><br><br><br><br><br><br>
            <form method="POST" action="../Controladores/login.php">
                <h3>Iniciar Sesion</h3> <br><br><br>
                <input type="number" class="form-control tamaño-input" name="inputDocumento" required="" placeholder="Documento de identidad"><br>
                <input type="password" class="form-control tamaño-input" name="inputContrasena" required="" placeholder="Contraseña"><br><br>
                <button type="submit" class="btn btn-light btn-tamaño">Ingresar</button>
            </form>
        </center><br><br><br><br>
                    
        <?php

        if (isset($_SESSION['message'])) { ?>

        <div class="alert alert-<?= $_SESSION['message_type'] ?> alert-dismissible fade show" role="alert">
            <?= $_SESSION['message'] ?>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>

        <?php session_unset();
        } ?>
    </div>

    <script src="jquery/dist/jquery.min.js"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>
</body>
</html>
<?php

require_once('../modelo/usuarios.php');
session_start();

if($_POST){
    $Documento = $_POST['inputDocumento'];
    $Password = $_POST['inputContrasena'];

    $Modelo = new Usuarios();
    if($Modelo->login($Documento, $Password)){
        $Modelo->validarSessionBibliotecario();
        $_SESSION['message'] = 'Sesión Iniciada';
            $_SESSION['message_type'] = 'success';
        
    }else{
        header('Location: ../Vista/index.php');
        $_SESSION['message'] = 'No se pudo iniciar sesión';
    $_SESSION['message_type'] = 'danger';
    }    
}

?>
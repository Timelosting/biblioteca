<?php

require_once('../../conexion.php');
require_once("../../Usuarios/Modelo/usuarios.php");
session_start();

class Prestamo extends Conexion{
    public function __construct(){
        $this->db = parent::__construct();
    }

    public function getFechaDevolucion($CodigoPrestamo){
        $FechaDevolucion = NULL;
        $statement=$this->db->prepare("SELECT datFechaDevolucion FROM tblPrestamos where intCodPrestamo = :CodigoPrestamo");
        $statement->bindParam(':CodigoPrestamo',$CodigoPrestamo);
        $statement->execute();
        $result[]=$statement->fetch();
        $FechaDevolucion=implode(" ",$result[0]);
        
        return $FechaDevolucion;

    }
    public function add($CodigoLibro,$Documento,$rows){
        date_default_timezone_set('America/Bogota');
        $Fecha=date("Y-m-d");
        $statement = $this->db->prepare("insert into tblPrestamos(bgnDocumento,intCodLibro,datFechaPrestamo)
        values(:Documento,:CodigoLibro,:Fecha)");
        $statement->bindParam(':Documento',$Documento);
        $statement->bindParam(':CodigoLibro',$CodigoLibro);
        $statement->bindParam(':Fecha',$Fecha);
        if($rows>0){
            if($statement->execute()){
                header('Location: ../../Bibliotecario/Vista/index.php');
                $_SESSION['message'] = 'Prestamo guardado';
                $_SESSION['message_type'] = 'success';
            }else{
                header('Location: ../../Bibliotecario/Vista/index.php');
                $_SESSION['message'] = 'No se pudo guardar el prestamo';
                $_SESSION['message_type'] = 'danger';
        }}else{
            header('Location: ../../Bibliotecario/Vista/index.php');
            $_SESSION['message'] = 'No Hay unidades disponibles para ese libro';
            $_SESSION['message_type'] = 'danger';
        }
    }

    public function getByCodigoLibro($Identificador){
        $rows=null;
        $statement=$this->db->prepare("SELECT * FROM tblPrestamos where intCodLibro=:Identificador");
        $statement->bindParam(':Identificador',$Identificador);
        $statement->execute();
        while ($result = $statement->fetch()) {
            $rows[]=$result;
        }
        return $rows;
    }
    public function getByDocumento($Identificador){
        $rows=null;
        $statement=$this->db->prepare("SELECT * FROM tblPrestamos where bgnDocumento=:Identificador");
        $statement->bindParam(':Identificador',$Identificador);
        $statement->execute();
        while ($result = $statement->fetch()) {
            $rows[]=$result;
        }
        return $rows;
    }

    public function updateDevolucion($CodigoPrestamo,$FechaDevolucion){
        date_default_timezone_set('America/Bogota');
        $Fecha=date("Y-m-d");
        $statement = $this->db->prepare("UPDATE tblPrestamos Set datFechaDevolucion = :Fecha where intCodPrestamo = :CodigoPrestamo");                                         
        $statement->bindParam(':Fecha',$Fecha);
        $statement->bindParam(':CodigoPrestamo',$CodigoPrestamo);
        if($FechaDevolucion==" "){
        if($statement->execute()){
            header('Location: ../../Bibliotecario/Vista/index.php');
            $_SESSION['message'] = 'Devolución guardada';
            $_SESSION['message_type'] = 'success';
        }
        else{
            header('Location: ../../Bibliotecario/Vista/index.php');
            $_SESSION['message'] = 'No se pudo guardar la devolución';
            $_SESSION['message_type'] = 'danger';
        }}else{
            header('Location: ../../Bibliotecario/Vista/index.php');
            $_SESSION['message'] = 'devolucion ya registrada';
            $_SESSION['message_type'] = 'danger';

        }
    }
    public function getMisPrestamos(){
        $modelo= new Usuarios();
        $Documento = $modelo->getDocumento();
        $rows=null;
        $statement = $this->db->prepare("SELECT * from tblPrestamos where bgnDocumento=:Documento");
        $statement->bindParam(':Documento',$Documento);
        $statement->execute();
        while ($result = $statement->fetch()) {
            $rows[]=$result;
        }
        return $rows;
    }
    public function getCodigoLibro($CodigoPrestamo){
        $CodigoLibro = NULL;
        $statement=$this->db->prepare("SELECT intCodLibro FROM tblPrestamos where intCodPrestamo = :CodigoPrestamo");
        $statement->bindParam(':CodigoPrestamo',$CodigoPrestamo);
        $statement->execute();
        $result=$statement->fetch();
        $CodigoLibros=$result;
        $CodigoLibro=array_sum($CodigoLibros);
        $CodigoLibro=$CodigoLibro/2;
        return $CodigoLibro;
    }
}

?>
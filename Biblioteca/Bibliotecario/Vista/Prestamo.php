<?php

require_once("../../Prestamo/Modelo/modelo.php");

$Modelo = new Prestamo();

?>
<!DOCTYPE html>
<html lang="es">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <link rel="stylesheet" href="..\..\bootstrap\css\bootstrap.min.css">

  <link rel="stylesheet" href="../../bootstrap/css/app.css">
  <title>Biblioteca</title>
</head>

<body>
  <nav class="navbar navbar-light bg-light fixed-top navbar-expand-md">
    <a href="index.php" class="navbar-brand posicion-search">Biblioteca</a>
    <div class="collapse navbar-collapse " id="listarNav">

      <ul class="navbar-nav ml-auto">
        <li class="navbar-item btn-light">
          <a href="index.php" class="nav-link">Volver</a>
        </li>
        <li class="navbar-item btn-light">
          <a href="..\..\index.php" class="nav-link">Cerrar Sesión</a>
        </li>
      </ul>
    </div>
  </nav><br>
 
  <table class="table">
    <thead class="thead-dark">
      <tr>
        <th scope="col">Id Prestamo</th>
        <th scope="col">Documento</th>
        <th scope="col">Id Libro</th>
        <th scope="col">Fecha de Prestamo</th>
        <th scope="col">Fecha de Devolucion</th>
      </tr>
    </thead>
    <tbody class="thead-light">
    <?php
    
    
        $Identificador = $_POST['identificador'];
        $Select =$_POST['selectBusqueda'];
        $Modelo = new Prestamo();
        if($Select=='documento'){
        $Prestamos=$Modelo->getByDocumento($Identificador);
        }
        else{
          $Prestamos=$Modelo->getByCodigoLibro($Identificador);
        }
        if($Prestamos != null){
        foreach($Prestamos as $prestamo){
        
    
      
    ?>
      <tr>
        <th><?php echo $prestamo['intCodPrestamo']?></th>
        <th><?php echo $prestamo['bgnDocumento']?></th>
        <th><?php echo $prestamo['intCodLibro']?></th>
        <th><?php echo $prestamo['datFechaPrestamo']?></th>
        <th><?php echo $prestamo['datFechaDevolucion']?></th>
      </tr>
    <?php
        }
      }
    ?>
    </tbody>
  </table>
  <script src="../../jquery/dist/jquery.min.js"></script>
  <script src="../../bootstrap/js/bootstrap.min.js"></script>

</body>

</html>
<?php

require_once("../../Libros/Modelo/modelo.php");
session_start();

$Modelo = new Libro();
?>
<!DOCTYPE html>
<html lang="es">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <link rel="stylesheet" href="..\..\bootstrap\css\bootstrap.min.css">

  <link rel="stylesheet" href="../../bootstrap/css/app.css">
  <title>Biblioteca</title>
</head>

<body>
  <nav class="navbar navbar-light bg-light fixed-top navbar-expand-md">
    <a href="#" class="navbar-brand posicion-search">Biblioteca</a>
    <div class="collapse navbar-collapse " id="listarNav">
      <ul class="navbar-nav ml-auto">
        <li class="navbar-item btn-light">
          <a href="#" class="nav-link" data-toggle="modal" data-target="#ModRegUsuario">Registrar Usuario</a>
        </li>
        <li class="navbar-item btn-light">
          <a href="#" class="nav-link" data-toggle="modal" data-target="#ModRegPrestamo">Registrar Prestamo</a>
        </li>
        <li class="navbar-item btn-light">
          <a href="#" class="nav-link" data-toggle="modal" data-target="#ModConPrestamo">Consultar Prestamo</a>
        </li>
        <li class="navbar-item btn-light">
          <a href="#" class="nav-link" data-toggle="modal" data-target="#ModRegDevolucion">Registrar Devolucion</a>
        </li>
        <li class="navbar-item btn-light">
          <a href="#" class="nav-link" data-toggle="modal" data-target="#ModRegLibro">Registrar Libro</a>
        </li>
      </ul>
      <ul class="navbar-nav ml-auto">
        <li class="navbar-item btn-light">
          <a href="..\..\index.php" class="nav-link">Cerrar Sesion</a>
        </li>
      </ul>
    </div>
  </nav><br>
  <?php

        if (isset($_SESSION['message'])) { ?>

        <div class="alert alert-<?= $_SESSION['message_type'] ?> alert-dismissible fade show" role="alert">
            <?= $_SESSION['message'] ?>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>

        <?php session_unset();
        } ?>
  <table class="table">
    <thead class="thead-dark">
      <tr>
        <th scope="col">Id</th>
        <th scope="col">Nombre</th>
        <th scope="col">Autor</th>
        <th scope="col">Año de Edicion</th>
        <th scope="col">Editorial</th>
        <th scope="col">Paginas</th>
        <th scope="col">Cantidad Disponible</th>
      </tr>
    </thead>
    <tbody class="thead-light">
    <?php
      $Libros=$Modelo->get();
      if($Libros != null){
        foreach($Libros as $libro){
    ?>
      <tr>
          <th><?php echo $libro['intCodLibro']?></th>
          <th><?php echo $libro['varNombreLibro']?></th>
          <th><?php echo $libro['varAutor']?></th>
          <th><?php echo $libro['intAnioEdicion']?></th>
          <th><?php echo $libro['varEditorial']?></th>
          <th><?php echo $libro['intNumPaginas']?></th>
          <th><?php echo $libro['intCantDisponible']?></th>
      </tr>
    <?php
        }
      }
    ?>
    </tbody>
  </table>
  
  <div class="modal fade ptopmod" id="ModRegUsuario" role="dialog"tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Registrar Usuario</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
        <form action="../../Usuarios/Controladores/add.php" method="post">
          <input name="inputDocIdentidad" required="" type="number" placeholder="Documento de Identidad" class="form-control"><br>
          <input name="inputNombre" required="" type="text" placeholder="Nombre Completo" class="form-control"><br>
          <input name="inputContrasena" required="" type="text" placeholder="Contraseña" class="form-control"><br>
          <select name="selectCargo" required=""  class="form-control">
            <option value="" >Seleccione el Cargo</option> 
            <option value="Estudiante">Estudiante</option>
            <option value="Docente">Docente</option>
            <option value="Otro">otro</option>
          </select><br>
          <input name="inputDireccion" required="" type="text" placeholder="Direccion de residencia" class="form-control"><br>
          <input name="inputTelefono" type="number" placeholder="Telefono" class="form-control"><br>
          <input name="inputCelular" required="" type="number" placeholder="Celular" class="form-control"><br>
          <input name="inputEmail" required="" type="email" placeholder="Correo electronico" class="form-control"><br>
          <select name="selectRol" required="" class="form-control">
            <option value="">Seleccione un Rol</option> 
            <option value="Bibliotecario">Bibliotecario</option>
            <option value="Cliente">Cliente</option>
          </select>
          <br>
          <div class="form-group justify-content-center" >
          <button type="submit" class="btn btn-primary btn-block form-control">Registrar</button>
        </div>
        </form>
        </div>
        
      </div>
    </div>
  </div>
  <div class="modal fade ptopmod" id="ModRegPrestamo" role="dialog"tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Registrar Prestamo</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
        <form action="../../Prestamo/Controladores/add.php" method="post">
          <input name="inputCodLibro" required="" type="number" placeholder="Codigo del Libro" class="form-control"><br>
          <input name="inputDocIdentidad" required="" type="number" placeholder="Documento de Identidad" class="form-control"><br>
        </div>
        <br>
        <div class="form-group justify-content-center" >
          <button type="submit" class="btn btn-primary btn-block form-control">Registrar</button>
        </div>
        </form>
      </div>
    </div>
  </div>
  <div class="modal fade ptopmod" id="ModConPrestamo" role="dialog"tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Consultar Prestamo</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form action="Prestamo.php" method="post">
        <div class="modal-body">
          <select name="selectBusqueda" required="" class="form-control">
            <option value="">Seleccione el metodo de busqueda</option> 
            <option value="codigoLibro">codigo del Libro</option>
            <option value="documento">Documento de Identidad</option>
          </select><br>
          <input name="identificador" required="" type="number" placeholder="Identificador" class="form-control"><br>
        </div>
        <div class="form-group justify-content-center" >
          <button type="submit" class="btn btn-primary btn-block form-control">Consultar</button>
        </div>
        </form>
      </div>
    </div>
  </div>
  <div class="modal fade ptopmod" id="ModRegDevolucion" role="dialog"tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Registrar Devolucion</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form action="../../Prestamo/Controladores/edit.php" method="post">
        <div class="modal-body">
          <input name="codPrestamo" required="" type="number" placeholder="Codigo del Prestamo" class="form-control"><br>
        </div>
        <div class="form-group justify-content-center" >
          <button type="submit" class="btn btn-primary btn-block form-control">Registrar</button>
        </div>
        </form>
      </div>
    </div>
  </div>
  <div class="modal fade ptopmod" id="ModRegLibro" role="dialog"tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Registrar Libro</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form action="../../Libros/Controladores/add.php" method="post">
        <div class="modal-body">
          <input name="nombre" required="" type="text" placeholder="Nombre" class="form-control"><br>
          <input name="autor" required="" type="text" placeholder="Autor" class="form-control"><br>
          <input name="anioEdicion"  type="number" placeholder="Año de la edicion" class="form-control"><br>
          <input name="editorial" type="text" placeholder="Editorial" class="form-control"><br>
          <input name="paginas" type="number" required="" placeholder="Paginas" class="form-control"><br>
          <input name ="unidades" type="number" required="" placeholder="Unidades" class="form-control">
        </div>
        <div class="form-group justify-content-center" >
          <button type="submit" class="btn btn-primary btn-block form-control">Registrar</button>
        </div>
        </form>
      </div>
    </div>
  </div>
  <script src="../../jquery/dist/jquery.min.js"></script>
  <script src="../../bootstrap/js/bootstrap.min.js"></script>
  
</body>

</html>
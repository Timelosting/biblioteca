
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
   
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">

    <link rel="stylesheet" href="bootstrap/css/app.css">
    <title>Biblioteca</title>
</head>
<body>
    <nav class="navbar navbar-light bg-light fixed-top navbar-expand-md">
        <a href="#" class="navbar-brand posicion-search">Biblioteca</a>
        <button class="navbar-toggler" type="button" data-toggle= "collapse" data-target="#listarNav" aria-expanded="false" aria-controls="navbarNAvDropDown"><span class="navbar-toggler-icon"></span></button>
        <div class="collapse navbar-collapse " id="listarNav">
          
            
            <ul class="navbar-nav ml-auto">
                </li>
                <li class="navbar-item">
                    <a href="Usuarios\Vista\index.php" class="nav-link">Iniciar Sesion</a>
                </li>
            </ul>
            
        </div>
    </nav>

    <script src="jquery/dist/jquery.min.js"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>
</body>
</html>
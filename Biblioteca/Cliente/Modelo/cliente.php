<?php

require_once('../../conexion.php');

class Cliente extends Conexion{
    public function __construct(){
        $this->db = parent::__construct();
    }

    public function add($Documento,$Nombre,$Password,$Cargo,$Direccion,$Telefono,$Celular,$Correo){
        $statement = $this->db->prepare("insert into tblUsuarios(bgnDocumento,varNombre,varClave,varCargo,varDireccion,
        intTelefono,bgnCelular,varCorreo,varRol)values(:Documento,:Nombre,:Password,:Cargo,:Direccion,:Telefono,:Celular,
        :Correo,'Cliente')");
        $statement->bindParam(':Documento',$Documento);
        $statement->bindParam(':Nombre',$Nombre);
        $statement->bindParam(':Password',$Password);
        $statement->bindParam(':Cargo',$Cargo);
        $statement->bindParam(':Direccion',$Direccion);
        $statement->bindParam(':Telefono',$Telefono);
        $statement->bindParam(':Celular',$Celular);
        $statement->bindParam(':Correo',$Correo);
        if($statement->execute()){
            header('Location: ../../Bibliotecario/Vista/index.php');
            $_SESSION['message'] = 'Usuario guardado';
            $_SESSION['message_type'] = 'success';
        }else{
            header('Location: ../Bibliotecario/Vista/index.php');
            $_SESSION['message'] = 'No se pudo registrar el usuario';
            $_SESSION['message_type'] = 'danger';
        }
    }
}

?>
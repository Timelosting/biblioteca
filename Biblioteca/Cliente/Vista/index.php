<?php

require_once("../../Libros/Modelo/modelo.php");

$Modelo = new Libro();
?>
<!DOCTYPE html>
<html lang="es">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <link rel="stylesheet" href="..\..\bootstrap\css\bootstrap.min.css">

  <link rel="stylesheet" href="../../bootstrap/css/app.css">
  <title>Biblioteca</title>
</head>

<body>
  <nav class="navbar navbar-light bg-light fixed-top navbar-expand-md">
    <a href="#" class="navbar-brand posicion-search">Biblioteca</a>
    <div class="collapse navbar-collapse " id="listarNav">
      
      <ul class="navbar-nav ml-auto">
        <li class="navbar-item btn-light">
          <a href="prestamos.php" class="nav-link">Mis Prestamos</a>
        </li>
        <li class="navbar-item btn-light">
          <a href="..\..\index.php" class="nav-link">Cerrar Sesión</a>
        </li>
      </ul>
    </div>
  </nav>
  <br>
  <hr>
  <table class="table">
    <thead class="thead-dark">
      <tr>
        <th scope="col">Id</th>
        <th scope="col">Nombre</th>
        <th scope="col">Autor</th>
        <th scope="col">Año de Edicion</th>
        <th scope="col">Editorial</th>
        <th scope="col">Paginas</th>
        <th scope="col">Cantidad Disponible</th>
      </tr>
    </thead>
    <tbody class="thead-light">
    <?php
      $Libros=$Modelo->get();
      if($Libros != null){
        foreach($Libros as $libro){
    ?>
      <tr>
          <th><?php echo $libro['intCodLibro']?></th>
          <th><?php echo $libro['varNombreLibro']?></th>
          <th><?php echo $libro['varAutor']?></th>
          <th><?php echo $libro['intAnioEdicion']?></th>
          <th><?php echo $libro['varEditorial']?></th>
          <th><?php echo $libro['intNumPaginas']?></th>
          <th><?php echo $libro['intCantDisponible']?></th>
      </tr>
    <?php
        }
      }
    ?>
    </tbody>
  </table>
  <script src="../../jquery/dist/jquery.min.js"></script>
  <script src="../../bootstrap/js/bootstrap.min.js"></script>
  
</body>

</html>
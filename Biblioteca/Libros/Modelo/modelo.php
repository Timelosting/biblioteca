<?php

require_once('../../conexion.php');
session_start();
class Libro extends Conexion{
    public function __construct(){
        $this->db = parent::__construct();
    }

    public function add($NombreLibro,$Autor,$AnioEdicion,$Editorial,$Paginas,$Unidades){
        $statement = $this->db->prepare("insert into tblLibros(varNombreLibro,varAutor,intAnioEdicion,varEditorial,
        intNumPaginas,intCantDisponible)values(:NombreLibro,:Autor,:AnioEdicion,:Editorial,:Paginas,:Unidades)");
        $statement->bindParam(':NombreLibro',$NombreLibro);
        $statement->bindParam(':Autor',$Autor);
        $statement->bindParam(':AnioEdicion',$AnioEdicion);
        $statement->bindParam(':Editorial',$Editorial);
        $statement->bindParam(':Paginas',$Paginas);
        $statement->bindParam(':Unidades',$Unidades);
        if($statement->execute()){
            header('Location: ../../Bibliotecario/Vista/index.php');
            $_SESSION['message'] = 'Libro guardado';
            $_SESSION['message_type'] = 'success';
        }else{
            header('Location: ../Bibliotecario/Vista/index.php');
            $_SESSION['message'] = 'No se pudo guardar el Libro';
            $_SESSION['message_type'] = 'danger';
        }
    }

    public function get(){
        $rows=null;
        $statement=$this->db->prepare("SELECT * FROM tblLibros ORDER BY varNombreLibro");
        $statement->execute();
        while ($result = $statement->fetch()) {
            $rows[]=$result;
        }
        return $rows;
    }

    public function getCantDisponible($CodigoLibro){
        $rows = NULL;
        $row = NULL;
        $statement=$this->db->prepare("SELECT intCantDisponible FROM tblLibros where intCodLibro = :CodigoLibro");
        $statement->bindParam(':CodigoLibro',$CodigoLibro);
        $statement->execute();
        $result=$statement->fetch();
        $row=$result;
        $rows=array_sum($row);
        $rows=$rows/2;
        return $rows;

    }

    public function updateMasUno($CodigoLibro,$rows,$FechaDevolucion){
        if($FechaDevolucion==" "){
            $a=$rows;
            $b=1;
            $cantMasUno=$a+$b;
            $statement = $this->db->prepare('UPDATE tblLibros Set intCantDisponible = :cantMasUno where intCodLibro=:CodigoLibro');
            $statement->bindParam(':cantMasUno',$cantMasUno);
            $statement->bindParam(':CodigoLibro',$CodigoLibro);
            $statement->execute();
        }
    }
    public function updateMenosUno($CodigoLibro,$rows){
        if($rows>0){
            $a=$rows;
            $b=1;
            $cantMasUno=$a-$b;
            $statement = $this->db->prepare('UPDATE tblLibros Set intCantDisponible = :cantMasUno where intCodLibro=:CodigoLibro');
            $statement->bindParam(':cantMasUno',$cantMasUno);
            $statement->bindParam(':CodigoLibro',$CodigoLibro);
            $statement->execute();
        }
    }
}

?>
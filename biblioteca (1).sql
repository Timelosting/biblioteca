-- phpMyAdmin SQL Dump
-- version 5.0.3
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 20-11-2020 a las 01:30:28
-- Versión del servidor: 10.4.14-MariaDB
-- Versión de PHP: 7.4.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `biblioteca`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbllibros`
--

CREATE TABLE `tbllibros` (
  `intCodLibro` int(11) NOT NULL,
  `varNombreLibro` varchar(50) NOT NULL,
  `varAutor` varchar(50) DEFAULT NULL,
  `intAnioEdicion` int(11) DEFAULT NULL,
  `varEditorial` varchar(50) DEFAULT NULL,
  `intNumPaginas` int(11) NOT NULL,
  `intCantDisponible` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `tbllibros`
--

INSERT INTO `tbllibros` (`intCodLibro`, `varNombreLibro`, `varAutor`, `intAnioEdicion`, `varEditorial`, `intNumPaginas`, `intCantDisponible`) VALUES
(1, 'Cien años de soledad', 'Gabriel Garcia Marquez', 1966, 'debolsillo', 471, 14),
(2, 'Popol vuh', 'Anonimo', 2010, 'Panamericana', 138, 5),
(3, 'Odisea', 'Homero', 2017, 'Austral', 448, 13),
(4, 'El principito', 'Saint-Exupéry, Antoine de', 2008, 'Salamandra', 96, 3),
(5, 'Diario de Anne Frank', 'Anne Frank', 2017, 'Debolsillo', 384, 5),
(6, 'Crónica de una muerte anunciada', 'Gabriel Garcia Marquez', 2017, 'Debolsillo', 144, 12),
(7, 'Moby-Dick', 'Herman Melville', 2020, 'Alma', 496, 0),
(8, 'Del amor y otros Demonios', 'Gabriel Garcia Marquez', 2020, 'Debolsillo', 176, 7);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tblprestamos`
--

CREATE TABLE `tblprestamos` (
  `intCodPrestamo` int(11) NOT NULL,
  `bgnDocumento` bigint(20) NOT NULL,
  `intCodLibro` int(11) NOT NULL,
  `datFechaPrestamo` date NOT NULL,
  `datFechaDevolucion` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `tblprestamos`
--

INSERT INTO `tblprestamos` (`intCodPrestamo`, `bgnDocumento`, `intCodLibro`, `datFechaPrestamo`, `datFechaDevolucion`) VALUES
(1, 100012345, 1, '2020-11-19', '0000-00-00'),
(2, 1000658843, 7, '2020-11-19', '0000-00-00'),
(3, 1000658843, 7, '2020-11-19', '0000-00-00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tblusuarios`
--

CREATE TABLE `tblusuarios` (
  `bgnDocumento` bigint(20) NOT NULL,
  `varNombre` varchar(50) NOT NULL,
  `varClave` varchar(50) NOT NULL,
  `varCargo` varchar(50) NOT NULL,
  `varDireccion` varchar(50) DEFAULT NULL,
  `intTelefono` int(11) DEFAULT NULL,
  `bgnCelular` bigint(20) DEFAULT NULL,
  `varCorreo` varchar(50) DEFAULT NULL,
  `varRol` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `tblusuarios`
--

INSERT INTO `tblusuarios` (`bgnDocumento`, `varNombre`, `varClave`, `varCargo`, `varDireccion`, `intTelefono`, `bgnCelular`, `varCorreo`, `varRol`) VALUES
(100012345, 'alejandro', '123', 'Estudiante', 'cl 5 # 3', 3009991, 3008883322, 'anonimo@gmail.com', 'Cliente'),
(1000415883, 'Santiago Calle', '12345', 'Estudiante', 'cll 39 #91-16', 2526274, 3155638972, 'santiago.calle5883@unaula.edu.co', 'Bibliotecario'),
(1000658843, 'Jose Uribe', '123', 'Profesor', 'cr 80 # 86- 13', 2528356, 3017236114, 'anonimo@anonimo.com', 'Bibliotecario');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `tbllibros`
--
ALTER TABLE `tbllibros`
  ADD PRIMARY KEY (`intCodLibro`);

--
-- Indices de la tabla `tblprestamos`
--
ALTER TABLE `tblprestamos`
  ADD PRIMARY KEY (`intCodPrestamo`),
  ADD KEY `bgnDocumento` (`bgnDocumento`),
  ADD KEY `intCodLibro` (`intCodLibro`);

--
-- Indices de la tabla `tblusuarios`
--
ALTER TABLE `tblusuarios`
  ADD PRIMARY KEY (`bgnDocumento`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `tbllibros`
--
ALTER TABLE `tbllibros`
  MODIFY `intCodLibro` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `tblprestamos`
--
ALTER TABLE `tblprestamos`
  MODIFY `intCodPrestamo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `tblprestamos`
--
ALTER TABLE `tblprestamos`
  ADD CONSTRAINT `tblprestamos_ibfk_1` FOREIGN KEY (`bgnDocumento`) REFERENCES `tblusuarios` (`bgnDocumento`),
  ADD CONSTRAINT `tblprestamos_ibfk_2` FOREIGN KEY (`intCodLibro`) REFERENCES `tbllibros` (`intCodLibro`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
